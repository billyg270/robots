package uk.co.cablepost.robots.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class RobotsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

    }
}
